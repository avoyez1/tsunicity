import hashlib
from pandas import DataFrame, Series
import pandas as pd
from joblib import Parallel, delayed

def _row_rounding(row, r: int):
    """
    Round the value to get the number of integer decimals

    Args:
        row ([type]): [description]
        r (int): [description]
    """
    return row.apply(lambda x: int(x) - int(x) % 10 ** r)

def rounding_raster_bis(df: pd.DataFrame, date_col: str, r: int, na: int = 0, n_jobs=1):
    """

    Args:
        df (DataFrame): Input DataFrame. Format :
            - 1 column for the date
            - 1 column per series
        r (int): [description]
        na (int) : Action to do with NaN values (replace by 0 by default)
        n_jobs (int, optional): [description]. Defaults to 1.

    Returns:
        [type]: [description]
    """
    if na is not None:
        df = df.fillna(value=na)

    ndf = df.loc[:, df.columns != date_col]

    if n_jobs == 1:
        ndf = ndf.apply(lambda row: _row_rounding(row, r))
    else:
        ndf = ndf.parallel_apply(lambda row: _row_rounding(row, r))

    ndf[date_col] = df[date_col]

    cols = list(df.columns)
    #cols = cols[-1:] + cols[:-1]
    ndf = ndf[cols]

    return ndf

def _uuid_row(row, k: int, hash=False):
    """
    Compute the UUID (hashed string) for all the columns of a DataFrame row.

    Args:
        row (dict): DataFrame row (dict)
        k (int): Number of columns consecutives columns to put in the UUID
        hash (bool, optional): If true : UUID as md5 hash. Defaults to False : UUID as string consumption concatenated.

    Returns:
        dict: DataFrame row with, for each columns the UUID.
    """
    vals = []
    names = []
    
    for start in range(0, len(row) - k - 1):
        s = ""
        for i in range(start, start + k):
            s += str(row[i])
         
        if hash:
            h = hashlib.blake2s()
            h.update(s.encode("utf-8"))
            s = h.hexdigest()
            
        vals.append(s)
        names.append(start)
    
    return pd.Series(vals, index=names)

def _compute_uuid(df: pd.DataFrame, k: int, hash=False, n_jobs=1):
    """
    Get, for each individuals and timestamps, UUID coresponding to a hash to the k consumptions after the timestamp.

    Args:
        df (pd.DataFrame): Time series dataframe (individuals as rows, timestamps as columns) 
        k (int): Number of columns consecutives columns to put in the UUID
        hash (bool, optional): If true : UUID as md5 hash. Defaults to False : UUID as string consumption concatenated.

    Returns:
        [pd.DataFrame]:
    """
    if n_jobs == 1:
        df_uuid = df.apply(lambda row: _uuid_row(list(row), k, hash), axis=1)
    else:    
        df_uuid = df.parallel_apply(lambda row: _uuid_row(list(row), k, hash), axis=1)

    df_uuid.columns = df.columns[: len(df_uuid.columns)]
    return df_uuid

def _anonset_group_column(df: pd.DataFrame, uid_col: str, col: str):
    uni_cols = ["uuid", "repet", "timestamp"]

    df_tmp = df[[uid_col, col]] \
                .sort_values(by=[col]) \
                .groupby(by=[col]) \
                .count().reset_index()

    df_tmp["timestamp"] = col
    df_tmp.columns = uni_cols

    return df_tmp

def _anonset_from_uuid(df: pd.DataFrame, uid_col: str, n_jobs=1):
    """
    Compute the anonymity set.
    Get for each UUID and timestamp the number of times this UUID is appearing.

    Args:
        df (pd.DataFrame): UUID DataFrame (as returned by the compute_uuid function)

    Returns:
        [pd.DataFrame]: 3 columns DataFrame :
            - uuid
            - timestamp
            - repet (number of times the UUID is appearing at the timestamp)
    """
    uni_cols = ["uuid", "repet", "timestamp"]
    df_unis = []

    if n_jobs == 1:
        for col in df.columns[1:]:
            """
            df_tmp = df[[uid_col, col]] \
                .sort_values(by=[col]) \
                .groupby(by=[col]) \
                .count().reset_index()

            df_tmp["timestamp"] = col
            df_tmp.columns = uni_cols
            """
            df_tmp = _anonset_group_column(df, uid_col, col)
            df_unis.append(df_tmp)
    else:
        df_unis = Parallel(n_jobs=n_jobs)(delayed(_anonset_group_column)(df, uid_col, col) for col in df.columns[1:])
    
    df_unis = pd.concat(df_unis)
    df_unis.timestamp = pd.to_datetime(df_unis.timestamp)
    
    return df_unis

def anonset_raster(df: pd.DataFrame, k: int, uid_col: str = "uid", hash: bool = False, n_jobs=1):
    """
    Compute anonymity sets uuids from a time series DataFrame.
    Call the compute_uuid then the unicity_from_uuid functions.

    Args:
        df (pd.DataFrame): Time series consumtion DataFrame
        k (int): Number of columns consecutives columns to put in the UUID
        hash (bool): If true : UUID as md5 hash. Defaults to False : UUID as string consumption concatenated.

    Returns:
        [pd.DataFrame]: 3 columns DataFrame :
            - uuid
            - timestamp
            - repet (number of times the UUID is appearing at the timestamp)
    """
    return _anonset_from_uuid(_compute_uuid(df, k, hash), uid_col)

def anonset_raster_bis(df: DataFrame, date_col: str, k: int, uid_col: str = 'uid', hash=False, n_jobs=1):
    """
    Compute anonymity set from raster dataframe

    Args:
        df (DataFrame): Input DataFrame. Format :
            - 1 column for the date
            - 1 column per series
        date_col (str): Name of the date information
        k (int): Number of points to group together
        hash (bool, optional): Use the hash group computation (usefull for large groups). Defaults to False.
    """
    ndf = df.loc[:, df.columns != date_col]
    uid = ndf.columns

    ndf = ndf.T
    ndf.columns = df[date_col]

    ndf[uid_col] = uid
    cols = ndf.columns.tolist()
    cols = cols[-1:] + cols[:-1]
    ndf = ndf[cols]

    return anonset_raster(ndf, k, uid_col, hash, n_jobs)

