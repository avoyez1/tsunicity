from datetime import time
from typing import List
import pandas as pd
import numpy as np
import hashlib
from bokeh.io import show
from bokeh.plotting import figure
import bokeh.models as bm
from pandas.core.frame import DataFrame
import seaborn as sns
import matplotlib.pyplot as plt

import tsunicity.compute as compute

class TsUnicity:
    """
    Unique number of k-columns time-series per timestamp
    dataframe format :
        - timestamp: Timestamp name
        - nuniques: Number of uniques points
        - total: Total number of points
        - proportion : nuniques / total
    """

    def __init__(self, df: DataFrame, k: int):
        self.k = k

        self.df = df

    @staticmethod
    def from_csv(path: str, sep: str, k: int, timestamp_col: str = "timestamp", nuniques_col: str = "nuniques", total_col: str = "total", proportion_col: str = "proportion", european_float=False) -> 'TsUnicity':
        """
        Load unicity statistics from the 

        Returns:
            [type]: [description]
        """
        df = pd.read_csv(path, sep=sep)

        df = df.rename(columns={
            timestamp_col: "timestamp",
            nuniques_col: "nuniques",
            total_col: "total",
            proportion_col: "proportion"
        })
    
        df = df.astype({
            "nuniques": "int32",
            "total": "int32",
        })

        if european_float:  # European convention (, float separator instead of .) => recompute the proportion it is easier
            df["proportion"] = df["nuniques"] / df["total"]
        else:
            df = df.astype({
                "proportion": "float32"
            })

        df["timestamp"] = pd.to_datetime(df["timestamp"])

        return TsUnicity(df, k)

    @staticmethod
    def merge(uncity_list: List['TsUnicity']) -> 'TsUnicity':
        """
        Merge multiple unicity object together.
        All the objects must have the same k value.
        """
        dfs = []
        k = None

        for i, uncity in enumerate(uncity_list):
            if i == 0:
                k = uncity.k
            else:
                assert k == uncity.k

            dfs.append(uncity.df)

        df = pd.concat(dfs)

        df = df.groupby(by="timestamp").sum()
        df["proportion"] = df["nuniques"] / df["total"]

        return TsUnicity(df, k, hash)


    def plot_count(self):
        """
        Plot the number of uniques UUID per timestamp 
        """
        p = figure(plot_width=800, plot_height=300,
                   x_axis_type="datetime", title="Number of UUIDs per timestamps"
                )
        p.xaxis.axis_label = 'Date-Time'
        p.yaxis.axis_label = 'Number of unique UUID'

        p.line(x=self.df.timestamp, y=self.df.nuniques, line_color="blue")

        show(p)

    def plot_proportion(self):
        """
        Plot the proportion of uniques UUID relatively to the total number of UUIDs
        """
        p = figure(plot_width=800, plot_height=300,
                   x_axis_type="datetime", title="Number of UUIDs per timestamps"
                )
        p.xaxis.axis_label = 'Date-Time'
        p.yaxis.axis_label = 'Number of unique UUID'

        p.line(x=self.df.timestamp, y=self.df.proportion, line_color="blue")

        show(p)

    
    def plot_mean_by_month(self):
        """
        Plot the mean number of UUIds per month
        """
        df_ags = self.df.groupby(pd.Grouper(key="timestamp", freq="M"))\
            .mean().reset_index()
        df_ags.timestamp = pd.to_datetime(df_ags.timestamp)

        p = figure(plot_width=800, plot_height=300,
                   x_axis_type="datetime", title="Mean number of UUIDs per month")
        p.xaxis.axis_label = 'Date-Time'
        p.yaxis.axis_label = 'Mean number of unique UUID'

        p.line(x=df_ags["timestamp"], y=df_ags["nuniques"], line_color="blue")

        show(p)

    def plot_mean_proportion_by_month(self):
        """
        Plot the mean proportion of UUIds per month
        """
        df_ags = self.df.groupby(pd.Grouper(key="timestamp", freq="M"))\
            .mean().reset_index()
        df_ags.timestamp = pd.to_datetime(df_ags.timestamp)

        p = figure(plot_width=800, plot_height=300,
                   x_axis_type="datetime", title="Mean number of UUIDs per month")
        p.xaxis.axis_label = 'Date-Time'
        p.yaxis.axis_label = 'Mean number of unique UUID'

        p.line(x=df_ags["timestamp"], y=df_ags["proportion"], line_color="blue")

        show(p)

    def boxplot_by_month(self):
        """
        Boxplot of the number of unique UUIDs by month
        """
        df_tmp = self.df.copy()
        df_tmp["month"] = df_tmp.timestamp.apply(lambda x: x.month)

        plt.subplots(figsize=(12, 5))
        ax = sns.boxplot(data=df_tmp.sort_values("month"), x="month", y="nuniques")
        ax.set_xticklabels(ax.get_xticklabels(), rotation=30)
        ax.set(xlabel="Month", ylabel="Unique number")

        return ax

    def boxplot_proportion_by_month(self):
        """
        Boxplot of the proportion of unique UUIDs by month
        """
        df_tmp = self.df.copy()
        df_tmp["month"] = df_tmp.timestamp.apply(lambda x: x.month)

        plt.subplots(figsize=(12, 5))
        ax = sns.boxplot(data=df_tmp.sort_values("month"), x="month", y="proportion")
        ax.set_xticklabels(ax.get_xticklabels(), rotation=30)
        ax.set(xlabel="Months", ylabel="Unique proportion")

        return ax

    def plot_mean_by_days(self):
        """
        Plot the mean number of UUIDs per day
        """
        df_ags = self.df.groupby(pd.Grouper(key="timestamp", freq="D"))\
            .mean().reset_index()
        df_ags.timestamp = pd.to_datetime(df_ags.timestamp)

        p = figure(plot_width=800, plot_height=300,
                   x_axis_type="datetime", title="Mean number of UUIDs per day")
        p.xaxis.axis_label = 'Date-Time'
        p.yaxis.axis_label = 'Mean number of unique UUID'

        p.line(x=df_ags["timestamp"], y=df_ags["nuniques"], line_color="blue")

        show(p)

    def plot_mean_proportion_by_days(self):
        """
        Plot the mean proportion of UUIDs per day
        """
        df_ags = self.df.groupby(pd.Grouper(key="timestamp", freq="D"))\
            .mean().reset_index()
        df_ags.timestamp = pd.to_datetime(df_ags.timestamp)

        p = figure(plot_width=800, plot_height=300,
                   x_axis_type="datetime", title="Mean number of UUIDs per day")
        p.xaxis.axis_label = 'Date-Time'
        p.yaxis.axis_label = 'Mean number of unique UUID'

        p.line(x=df_ags["timestamp"], y=df_ags["proportion"], line_color="blue")

        show(p)

    def plot_mean_by_weekday(self):
        """
        Plot the mean number of UUIDs per weekday
        """
        df_ags = self.df.groupby(self.df.timestamp.dt.weekday) \
            .mean()
        df_ags["days"] = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
        
        df_ags.plot(x="days", y="nuniques")

    def plot_mean_proportion_by_weekday(self):
        """
        Plot the mean proportion of UUIDs per weekday
        """
        df_ags = self.df.groupby(self.df.timestamp.dt.weekday) \
            .mean()
        df_ags["days"] = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
        
        df_ags.plot(x="days", y="proportion")

    def boxplot_by_weekday(self):
        """
        Plot a seaborn boxplot with the number of unique UUIDs per weekday
        """
        df_wd = self.df.copy()
        df_wd["day_name"] = df_wd.timestamp.apply(lambda x: x.day_name())
        df_wd["day_idx"] = df_wd.timestamp.apply(lambda x: x.weekday())

        plt.subplots(figsize=(12, 8))
        ax = sns.boxplot(data=df_wd.sort_values("day_idx"), x="day_name", y="nuniques")
        ax.set(xlabel="Week days", ylabel="Unique number")

        return ax

    def boxplot_proportion_by_weekday(self):
        """
        Plot a seaborn boxplot with the proportion of unique UUIDs per weekday
        """
        df_wd = self.df.copy()
        df_wd["day_name"] = df_wd.timestamp.apply(lambda x: x.day_name())
        df_wd["day_idx"] = df_wd.timestamp.apply(lambda x: x.weekday())

        plt.subplots(figsize=(12, 8))
        ax = sns.boxplot(data=df_wd.sort_values("day_idx"), x="day_name", y="proportion")
        ax.set(xlabel="Week days", ylabel="Unique proportion")

        return ax

    def plot_mean_by_hour(self):
        """
        Plot the mean number of UUIDs per day hours
        """
        df_ags = self.df.groupby(self.df.timestamp.dt.hour).mean().reset_index()

        p = figure(plot_width=800, plot_height=300, title="Mean number of UUIDs per hours")
        p.xaxis.axis_label = 'Time'
        p.yaxis.axis_label = 'Mean number of unique UUID'

        p.line(x=df_ags["timestamp"], y=df_ags["nuniques"], line_color="blue")

        show(p)

    def plot_mean_proportion_by_hour(self):
        """
        Plot the mean proportion of UUIDs per day hours
        """
        df_ags = self.df.groupby(self.df.timestamp.dt.hour).mean().reset_index()

        p = figure(plot_width=800, plot_height=300, title="Mean number of UUIDs per hours")
        p.xaxis.axis_label = 'Time'
        p.yaxis.axis_label = 'Mean number of unique UUID'

        p.line(x=df_ags["timestamp"], y=df_ags["proportion"], line_color="blue")

        show(p)

    def boxplot_by_hour(self):
        """
        Boxplot of the number of unique UUIDs per hour
        """
        df_tmp = self.df.copy()
        df_tmp["time"] = df_tmp.timestamp.apply(lambda x: x.hour)

        plt.subplots(figsize=(12, 5))
        ax = sns.boxplot(data=df_tmp.sort_values("time"), x="time", y="nuniques")
        ax.set_xticklabels(ax.get_xticklabels(), rotation=30)
        ax.set(xlabel="Time (hours)", ylabel="Unique number")

        return ax

    def boxplot_proportion_by_hour(self):
        """
        Boxplot of the proportion of unique UUIDs per hour
        """
        df_tmp = self.df.copy()
        df_tmp["time"] = df_tmp.timestamp.apply(lambda x: x.hour)

        plt.subplots(figsize=(12, 5))
        ax = sns.boxplot(data=df_tmp.sort_values("time"), x="time", y="proportion")
        ax.set_xticklabels(ax.get_xticklabels(), rotation=30)
        ax.set(xlabel="Time (hours)", ylabel="Unique proportion")

        return ax

    def plot_mean_by_minute(self):
        """
        Plot the mean number of UUIDs per day minutes
        """
        df_ags = self.df.groupby(self.df.timestamp.dt.time).nuniques.mean().reset_index()
        
        p = figure(plot_width=800, plot_height=300,
                   x_axis_type="datetime", title="Mean number of UUIDs per minutes")
        p.xaxis.axis_label = 'Time (minutes)'
        p.yaxis.axis_label = 'Mean number of unique series'

        p.line(x=df_ags["timestamp"], y=df_ags["nuniques"], line_color="blue")

        show(p)

    def plot_mean_proportion_by_minute(self):
        """
        Plot the mean proportion of UUIDs per day minutes
        """
        df_ags = self.df.groupby(self.df.timestamp.dt.time).proportion.mean().reset_index()
        
        p = figure(plot_width=800, plot_height=300,
                   x_axis_type="datetime", title="Mean proportion of unique UUIDs per minutes")
        p.xaxis.axis_label = 'Time (minutes)'
        p.yaxis.axis_label = 'Mean proportion of unique series'

        p.line(x=df_ags["timestamp"], y=df_ags["proportion"], line_color="blue")

        show(p)

    def boxplot_by_minute(self):
        """
        Boxplot of the number of unique UUIDs per minutes
        """
        df_tmp = self.df.copy()
        df_tmp["time"] = df_tmp.timestamp.apply(lambda x: x.time())

        plt.subplots(figsize=(12, 5))
        ax = sns.boxplot(data=df_tmp.sort_values("time"), x="time", y="nuniques")
        ax.set_xticklabels(ax.get_xticklabels(), rotation=30)
        ax.set(xlabel="Time (minutes)", ylabel="Unique number.")

        return ax

    def boxplot_proportion_by_minute(self):
        """
        Boxplot of the proportion of unique UUIDs per minutes
        """
        df_tmp = self.df.copy()
        df_tmp["time"] = df_tmp.timestamp.apply(lambda x: x.time())

        plt.subplots(figsize=(12, 5))
        ax = sns.boxplot(data=df_tmp.sort_values("time"), x="time", y="proportion")
        ax.set_xticklabels(ax.get_xticklabels(), rotation=30)
        ax.set(xlabel="Time (minutes)", ylabel="Unique proportion.")

        return ax

def unicity_boxplot_multiple_k(unicities: List[TsUnicity]):
    dfs = []

    for unicity in unicities:
        df = unicity.df.copy()
        df["k"] = unicity.k
        dfs.append(df)

    df_k = pd.concat(dfs)

    ax = sns.boxplot(data=df_k, x="k", y="proportion")
    return ax


class TsAnonsets:
    """
    Time Series anonimity set.
    Handle a dataframe of the following format :
        - UUID
        - timestamp
        - repet
    """

    def __init__(self, df_uni:DataFrame, k: int, hash=False):
        """Do not use directly.
        Use 'from_raster' or 'from_raster_bis' instead.

        Args:
            df_uni (DataFrame): [description]
            k (int): [description]
            hash (bool, optional): [description]. Defaults to False.
        """
        self.hash = hash
        self.k = k
        self.df_uni = df_uni

    @staticmethod
    def from_raster(df: DataFrame, k: int, uid_col: str, hash=False, n_jobs=1) -> 'TsAnonsets':
        df_uni = compute.anonset_raster(df, k, uid_col, hash, n_jobs)
        return TsAnonsets(df_uni, k, hash)
    
    @staticmethod
    def from_raster_bis(df: DataFrame, date_col: str, k: int, uid_col: str = 'uid', hash: bool = False, n_jobs=1) -> 'TsAnonsets':
        df_uni = compute.anonset_raster_bis(df, date_col, k, uid_col, hash, n_jobs)
        return TsAnonsets(df_uni, k, hash)

    @staticmethod
    def merge(anonsets: List['TsAnonsets']) -> 'TsAnonsets':
        """
        Merge two anonsets together.
        All the datasets mush share the same value of k and hash.
        The 'df' field of the new TsAnonsets class will be set to None.

        Args:
            anonsets ()
        """
        k = None
        hash = None
        df_grouped = []

        for i, anon in enumerate(anonsets):
            if i == 0:
                k = anon.k
                hash = anon.hash
            else:
                assert k == anon.k
                assert hash == anon.hash

            df_grouped.append(anon.df_uni)

        df_grouped = pd.concat(df_grouped)

        df_grouped = df_grouped.groupby(by=["timestamp", "uuid"]).sum().reset_index()

        return TsAnonsets(df_grouped, k, hash)

    def describe(self):
        return self.df_uni.repet.describe()

    def filter_zeros(self, zeros:str ="0.0") -> 'TsAnonsets':
        """
        Filter the zeros UUIds in the unicity DataFrame

        Args:
            zeros (str, optional): String representation of a zero consumption. Defaults to "0.0".

        Returns:
            TsAnonsets: A TsAnonsets with df_uni filtered of zeros
        """
        zeros = zeros * self.k

        if self.hash:
            h = hashlib.md5()
            h.update(zeros.encode("utf-8"))
            zeros = h.hexdigest()
        
        df_zeros = self.df_uni[self.df_uni.uuid != zeros]

        return TsAnonsets(df_zeros, self.k, self.hash)

    def uniques_by_ts(self) -> TsUnicity:
        """
        Get the number of uniques UUID per timestamp in a TsUnicity class
        """
        df_len = self.df_uni.groupby('timestamp').sum().reset_index()

        # Filtering and counting uniques series per timestamp
        df_uniq = self.df_uni[self.df_uni.repet == 1]
        df_ags = df_uniq.groupby("timestamp")\
            .count().reset_index()
        
        # Renaming and ordering the columns
        df_ags = df_ags[["timestamp", "repet"]]
        df_ags.columns = ["timestamp", "nuniques"]
        
        # Add the total count of all series per timestamp
        df_ags["total"] = df_len["repet"]

        # Compute the proportion of uniques series compared to the total number of series
        df_ags["proportion"] = df_ags["nuniques"] / df_ags["total"]

        # Making sure timestamp is a datetime
        df_ags.timestamp = pd.to_datetime(df_ags.timestamp)
        return TsUnicity(df_ags, self.k) #, self.hash)

    def plot_mean_group_size(self):
        """
        Plot the mean group size for each timestamp
        """
        df_ts = self.df_uni.groupby("timestamp").mean().reset_index()

        p = figure(plot_width=800, plot_height=300, x_axis_type="datetime")
        p.xaxis.axis_label = 'Time'
        p.yaxis.axis_label = 'Mean number of unique series'

        p.line(x=df_ts["timestamp"], y=df_ts["repet"], line_color="blue")

        show(p)

    def plot_max_group_size(self):
        """
        Plot the max group size for each timestamp
        """
        df_ts = self.df_uni.groupby("timestamp").max().reset_index()
        
        p = figure(plot_width=800, plot_height=300, x_axis_type="datetime")
        p.xaxis.axis_label = 'Date-Heure'
        p.yaxis.axis_label = 'Maximum number of unique series'

        p.line(x=df_ts["timestamp"], y=df_ts["repet"], line_color="blue")

        show(p)

    def plot_repetitions(self):
        """
        Plot the number of times a UUID is repeated (x axis) versus the number of UUIDs concerned (y axis) 
        """
        return self.df_uni.plot.hist(y="repet", bins=100)



